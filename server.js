var express = require("express");
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var requestJson = require('request-json');

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
var mlabAPIKey = "apiKey=l-3CuGaX_bvhn0XaqvACGc_B9ccb8P1V";

// Escuchamos en el puerto
app.listen(port);
console.log("API escuchando en el puerto " + port);


//Obtiene todos los usuarios dados de alta dentro de la colección MongoDB usuarios
app.get('/usuarios',
   function(req, res)
   {
     console.log("GET /usuarios");
     httpClient = requestJson.createClient(baseMlabURL);
     console.log("cliente creado");

     httpClient.get("usuarios?" + mlabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : {
               "msg":"Error obteniendo usuarios"
          }
          res.send(response);
        }
       )
   }
);

//Devuelve el Usuario pasado por id de la colección MongoDB
app.get('/usuarios/:id',
 function(req, res) {

   console.log("GET /usuarios/:id");

   var idUsuario = req.params.id;
   var query = 'q={"idUsuario" : ' + idUsuario + '}';

   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Usuario pedido");

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Dar de alta un usuario
app.post('/usuario',
  function(req, res) {

    console.log("POST /usuario");

    var query = 's={"idUsuario": -1}'
    //var query = 'q={"idUsuario" : 1}';

    console.log ("usuarios?" + query + "&" + mlabAPIKey);
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
      function(err, resMLab, body) {
          console.log("Ultimo usuario:" + body)
          if (body.length > 0) {
            console.log ("Obtenemos ultimo idUsuario: " + body[0].idUsuario);
            var idUsuario = parseInt(1) + parseInt(body[0].idUsuario);
            console.log ("idUsuario a intertar: " + idUsuario);
            var usuario = {
                "idUsuario" : idUsuario,
                "email"     : req.body.email,
                "password"  : req.body.password,
                "lastName"  : req.body.lastName,
                "firstName" : req.body.firstName,
             }
            httpClient.post("usuarios?" + "&" + mlabAPIKey, usuario,
              function(errPUT, resMLabPUT, bodyPOST) {
                console.log("POST realizado");
                var response = {
                "msg" : "Usuario dado de alta",
                }
                res.send(response);
              }
            );
          } else {
            response = {
              "msg" : "Id no encontrado"
            };
            res.send(response);
          }
        }
    );
  }
);

//Devuelve las cuentas de un usuario pasado por id
app.get('/usuarios/:id/cuentas',
 function(req, res) {

   console.log("GET /usuarios/:id/cuentas");

   var idUsuario = req.params.id;
   var query = 'q={"idUsuario" : ' + idUsuario + '}';
   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Cuenta pedida");

   httpClient.get("cuentas?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Devuelve los movimientos de una cuenta
app.get('/cuenta/:id/movimientos/',
 function(req, res) {

   console.log("GET /cuenta/:id/movimientos/");

   var idCuenta = req.params.id;
   var query = 'q={"idCuenta" : ' + idCuenta + '}';
   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Movimientos pedidos");

   httpClient.get("movimientos?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movimientos."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Movimientos no encontrados."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Dar de alta un movimiento de una cuenta
app.post('/cuenta/:id/movimiento/',
  function(req, res) {

    console.log("/cuenta/:id/movimiento/");

    //var query = 'q={ $sort: { "idMovimiento": -1} }'
    var query = 'q={"idMovimiento" : 1}';

    console.log ("movimientos?" + query + "&" + mlabAPIKey);
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("movimientos?" + query + "&" + mlabAPIKey,
      function(err, resMLab, body) {
          if (body.length > 0) {
            console.log ("Obtenemos ultimo idMovimiento: " + body[0].idMovimiento);
            var idMovimiento = parseInt(1) + parseInt(body[0].idMovimiento);
            console.log ("idMovimiento a intertar: " + idMovimiento);
            var nuevomovimiento = {
                "idMovimiento"   : idMovimiento,
                "importe"        : req.body.importe,
                "fechaMovimiento": req.body.fechaMovimiento,
                "horaMovimiento" : req.body.horaMovimiento,
                "desMovimiento"  : req.body.desMovimiento,
                "idCuenta"       : req.params.id,
             }
            httpClient.post("movimientos?" + "&" + mlabAPIKey, nuevomovimiento,
              function(errPUT, resMLabPUT, bodyPOST) {
                console.log("POST realizado");
                var response = {
                "msg" : "Movimiento dado de alta",
                }
                res.send(response);
              }
            );
          } else {
            response = {
              "msg" : "Id no encontrado"
            };
            res.send(response);
          }
        }
    );
  }
);


//HACER LOGIN USUARIO
//Pone logged=true, en el registro que corresponde al usuario y password pasado
// por el body, dentro de la colección de mongo dada de alta por MLab
app.post('/login',
 function(req, res) {

   console.log("POST /projectleti/login");

   var email = req.body.email;
   var password = req.body.password;
   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';

   console.log("query es: " + query);

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Actualiza estado");
         query = 'q={"idUsuario" : ' + body[0].idUsuario +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("usuarios?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT realizado");
             var response = {
               "msg" : "Usuario logado con éxito",
               "Usuario" : body[0]
             }
             res.send(response);
           }
         );
       }
     }
   );
 }
);

//HACER LOGOUT USUARIO
//Quita logged=true, en el registro que corresponde al id pasado
// por parametro, dentro de la colección de mongo dada de alta por MLab
app.post('/logout/:id',
 function(req, res) {

   console.log("POST /projectleti/logout/:id");

   var query = 'q={"idUsuario": ' + req.params.id + '}';

   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Actualiza estado");
         query = 'q={"idUsuario" : ' + body[0].idUsuario +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("usuarios?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT realizado");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].idUsuario
             }
             res.send(response);
           }
         );
       }
     }
   );
 }
);
